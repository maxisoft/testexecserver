#!/usr/bin/env python

from broadcast import BroadCastThread
from CMDServer import CmdServerThread
import atexit
from install import install_startup

if __name__ == "__main__":
	install_startup()
	broadcast = BroadCastThread()
	broadcast.start()

	CMDServer = CmdServerThread()
	CMDServer.start()

	def at_exit_fct():
		broadcast.work = False
		CMDServer.work = False
		broadcast.join(2)
		CMDServer.join(2)
		try:  # force stop
			broadcast._Thread__stop()
			CMDServer._Thread__stop()
		except:
			pass
		del broadcast
		del CMDServer

	atexit.register(at_exit_fct)

	broadcast.join()
	CMDServer.join()
