import rsa

pub, priv = rsa.newkeys(2048*2)
with open('pub.rsa', 'w') as f:
	f.write(pub.save_pkcs1())
with open('priv.rsa', 'w') as f:
	f.write(priv.save_pkcs1())


