import os
from os.path import expanduser
import sys
import socket
import datetime
import rsa

__all__ = ["HOME_DIR", "INSTALL_DIR", "CMD_SERVER_PORT", "BROADCAST_PORT", "PUBKEY"]

HOME_DIR = expanduser("~")
INSTALL_DIR = os.path.dirname(sys.argv[0]) or os.getcwd()

_BASE_BROADCAST_PORT = 54925
_BASE_CMD_SERVER_PORT = 54926

PUBKEY = """-----BEGIN RSA PUBLIC KEY-----
MIIBCgKCAQEAkk/4sLR89jY0YzdqjkjJGryoQ/kwukeCugWE1p59oLTl3tUXJCOh
X+PxWvojktRPqKBPvtCqplxvNFIrRWTApE/BShwxb/kkBph+X3Y8RGN2dObbeU95
zDzxcFu8wGWja7LdukijCWkDTfvKl2CCoLnZwSnhWff8qawsyIZfFU1PxYwcE6tT
VOokpfud73bZ0RyxPsxBycwsMgNw7hU8WR7hH1bd31yHc8I7/3C9M+EtbjvHrVq1
YA7lVpgS2LDwkJ2CnaiGF8kl8LHyLSahhZgr2CUVDiSZirzym+QAc5PFCNA13bfG
Rp2MlirSYl550kTfCiUOoAuqqQG+zBBANQIDAQAB
-----END RSA PUBLIC KEY-----
"""

PUBKEY = rsa.PublicKey.load_pkcs1(PUBKEY)


def get_today_port():
	dt = datetime.date.today()
	return _BASE_BROADCAST_PORT + (sum(dt.isocalendar()) % 255)


def get_good_server_port(baseport, limit=500, excluded=tuple()):
	for i in xrange(limit):
		tmp = baseport + i
		if tmp in excluded:
			continue
		s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		try:
			s.bind(("", tmp))
		except socket.error:
			pass
		else:
			return tmp
		finally:
			s.close()


BROADCAST_PORT = get_today_port()
CMD_SERVER_PORT = get_good_server_port(_BASE_CMD_SERVER_PORT, excluded=(BROADCAST_PORT,))
