import select
import socket
import zlib
try:
	import cPickle as pickle
except ImportError:
	import pickle
from theglobals import *

port = BROADCAST_PORT
bufferSize = 1024 * 10

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.bind(("", port))
s.setblocking(0)

while True:
	result = select.select([s], [], [])
	data = result[0][0].recv(bufferSize)
	data = zlib.decompress(data)
	msg = pickle.loads(data)
	print msg