import threading
import socket
import select
import zlib
import sys
from theglobals import *

import rsa
try:
	import cPickle as pickle
except ImportError:
	import pickle


class CmdServerThread(threading.Thread):
	BUFFER_SIZE = (1024 * 5) ** 2

	def __init__(self, group=None, target=None, name=None, args=(), kwargs=None, verbose=None):
		super(CmdServerThread, self).__init__(group, target, name, args, kwargs, verbose)
		self.daemon = True
		self.s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		self.s.bind(("", CMD_SERVER_PORT))
		self.s.setblocking(0)
		self.work = True

	def run(self):
		while self.work:
			result = select.select([self.s], [], [])
			data = result[0][0].recv(CmdServerThread.BUFFER_SIZE)

			try:
				data = zlib.decompress(data)
				data, signature = pickle.loads(data)
				rsa.verify(data, signature, PUBKEY)
			except:
				continue

			def run(s):
				try:
					d = dict(locals(), **globals())
					exec s in d, d
				except Exception, e:
					print >> sys.stderr, e

			t = threading.Thread(target=run, args=(data,))
			t.start()

	def __del__(self):
		self.s.close()
