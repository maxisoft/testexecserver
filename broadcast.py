import threading
import time
from socket import *
import platform
import getpass
from get_my_ip import get_lan_ip
import zlib

try:
	import cPickle as pickle
except ImportError:
	import pickle

from theglobals import *


class BroadCastThread(threading.Thread):

	def __init__(self, group=None, target=None, name=None, args=(), kwargs=None, verbose=None):
		super(BroadCastThread, self).__init__(group, target, name, args, kwargs, verbose)
		self.daemon = True
		self.s = socket(AF_INET, SOCK_DGRAM)
		self.s.bind(('', 0))
		self.s.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)
		self.ip = get_lan_ip()
		self.work = True

	def run(self):
		while self.work:
			data = pickle.dumps({'platform': platform.uname(),
			                     'user': getpass.getuser(),
			                     'ip': self.ip,
			                     'port': CMD_SERVER_PORT,
			                     'time': time.time()})
			data = zlib.compress(data, 9)
			self.s.sendto(data, ('<broadcast>', BROADCAST_PORT))
			time.sleep(25)

	def __del__(self):
		self.s.close()